import Mock from 'mockjs'

Mock.mock('http://127.0.0.1:8998/api/Baseconfig/getInfo?v=1', (req, res) => {
	return {"status":200,"data":{"site_title":"GPT专业版","logo":"http:\/\/127.0.0.1:8787\/uploads\/admin\/202304\/2a809928431534277b503c18911e09b3.png","keyword":["oneadmin","快速生成","不写一行代码"],"descrip":"你好，欢迎体验智财小屋！我是人工智能胖大海，并经过至今最新的财税法律方面专门训练，因此我可以回答你在财税领域所有的问题，如您有使用问题，可以查阅个人中心-使用指南！","copyright":"智财小屋©2023","filesize":"100","filetype":"gif,png,jpg,jpeg,doc,docx,xls,xlsx,csv,pdf,rar,zip,txt,mp4,flv,wgt","water_status":"0","water_position":"5","domain":"http:\/\/127.0.0.1:8998","water_alpha":90,"free_num":"100","is_share":"1","share_reward_num":"10","ai_chat_note":"你好，我是人工智能胖大海！我可以回答你所有的问题，例如，你可以这样问我：<br>1、我公司在地级市，每月安排部分财务人员去省会城市参加培训，外出培训人员的差旅费用可以计入企业职工教育经费税前扣除吗？<br>2、个体工商户被列入经营异常，要怎么处理？<br>3、有一张excel表格，需要将T列的值全部取出，并以“,”分隔，请给出实际的例子。<br>4、我是一名企业财务会计，请你为我本周工作的周报列一个提纲并完善它，我本周的工作主要有原始凭证审核、编制记账凭证、登记明细分类账、月末计提、摊销、结转记账凭证，汇总、结账、对账和编制会计报表。","home_notice":"【通知】欢迎使用智税咨询小程序，新用户免费获得100次对话次数，免费额度消耗完毕后可联系客服获取兑换码或分享好友获取对话次数。","proxy_server":"http:\/\/oai.cntax.chat","proxy_type":"1","content_security":"1","wxGzhPay":"1","wxGzhLogin":"1","mpPay":"1"},"models":[{"assistant_id":1,"title":"AI聊天","description":"聊什么，你说了算","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/chat.png","sortid":1,"create_time":1680928145,"tag":"热门","status":1,"first_message":"你好，我是你的AI助手。"},{"assistant_id":6,"title":"心理咨询师","description":"AI树洞，畅所欲言，我将为您提供指导和建议，以管理你的情绪、压力、焦虑和其他心理健康问题。","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/dongman.png","sortid":2,"create_time":1680954218,"tag":"火爆","status":1,"first_message":"您好，我是AI心理咨询师，我将为您提供指导和建议，以管理你的情绪、压力、焦虑和其他心理健康问题。"},{"assistant_id":3,"title":"写作助手","description":"您可以提供写作的内容，我将帮助您改进文本，润色","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/poetry.png","sortid":3,"create_time":1680928356,"tag":null,"status":1,"first_message":"你好，我是你的写作助手。"},{"assistant_id":4,"title":"财税会计","description":"我将协助你处理公司和个人财务","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/weekly.png","sortid":4,"create_time":1680928393,"tag":null,"status":1,"first_message":"你好，我是你的AI财务。"},{"assistant_id":5,"title":"法律专家","description":"我将为你提供法律分析和提供建议","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/zaixianwenda.png","sortid":5,"create_time":1680928425,"tag":null,"status":1,"first_message":"你好，我是你的AI法律专家，任何法律问题都可向我咨询。"},{"assistant_id":2,"title":"AI程序员","description":"你可以提出编程相关问题，我将用计算机科学、网络安全等知识为你解答。","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/coder.png","sortid":6,"create_time":1680928258,"tag":null,"status":1,"first_message":"你好，我是AI程序员。"},{"assistant_id":7,"title":"AI金融分析师","description":"AI模型分析金融数据","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/Monitor%2C%20graph%2C%20growth%2C%20finance.png","sortid":7,"create_time":1680957035,"tag":"热门","status":1,"first_message":"你好，我是AI金融分析师。"},{"assistant_id":8,"title":"AI周报","description":"根据工作性质及内容，快速生成工作周报。","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/REPORT.png","sortid":8,"create_time":1680957104,"tag":"","status":1,"first_message":"你好，请描述本周工作内容，我将为你完善本周工作周报。"}],"main_model":{"assistant_id":9,"title":"智税咨询胖大海","description":"你好，欢迎体验智财小屋！我是人工智能胖大海，并经过至今最新的财税法律方面专门训练，因此我可以回答你在财税领域所有的问题，如您有使用问题，可以查阅个人中心-使用指南！","icon":"https:\/\/qnoss.bazhuayu.nauzone.cn\/gpt\/vip\/jqr.png","sortid":0,"create_time":1680957608,"tag":"","status":1,"first_message":"你好，我是人工智能胖大海！我可以回答你所有的问题，例如，你可以这样问我：<br>1、我公司在地级市，每月安排部分财务人员去省会城市参加培训，外出培训人员的差旅费用可以计入企业职工教育经费税前扣除吗？<br>2、个体工商户被列入经营异常，要怎么处理？<br>3、有一张excel表格，需要将T列的值全部取出，并以“,”分隔，请给出实际的例子。<br>4、我是一名企业财务会计，请你为我本周工作的周报列一个提纲并完善它，我本周的工作主要有原始凭证审核、编制记账凭证、登记明细分类账、月末计提、摊销、结转记账凭证，汇总、结账、对账和编制会计报表。"}}
})

Mock.mock('http://127.0.0.1:8998/api/Member/checkNum', () => {
	return {'status':200,'msg':'查询成功','data':{'num':100}}
})

Mock.mock('http://127.0.0.1:8998/api/Content/index', () => {
	return {'status':200,'data':{'total':1,'per_page':20,'current_page':1,'last_page':1,'data':[{'content_id':4,'type':3,'title':'如何更好使用本产品？','create_time':1679200292,'status':1}]}}
})

Mock.mock('http://127.0.0.1:8998/api/Content/detail', (req) => {
	return {'status':200,'data':{'content_id':4,'type':3,'title':'如何更好使用本产品？','content':'<p style=\'text-align: justify; line-height: 1.5;\'>首先需要转变一个认知，智税咨询不同于传统的搜索引擎，<span style=\'font-size: 1em;\'>它更像是你的一个私人助理，可以帮你解决很多文本创意相关的问题，也可以帮你检索收集知识，对文本的理解、归纳、整理、发散才是他的优势，简而言之，<\/span><strong><span style=\'font-size: 1em;\'><em style=\'font-size: 1em;\'>搜索引擎只能检索存在的信息，<\/em><em style=\'font-size: 1em;\'>智税咨询<\/em><em style=\'font-size: 1em;\'>可以为你创造信息。<\/em><\/span><\/strong><\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>&nbsp;<\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>你如何使用他，他就有多大的威力，本产品的使用方式主要以问答形式进行，<strong style=\'font-size: 1em;\'>我们提的问题<\/strong><strong style=\'font-size: 1em;\'>需要做到以下几点：<\/strong><\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>1.清晰明确:要让智税咨询明确你的问题或要求，确保你的问题表达清晰，简短明了。<br>2.具体细节:尽可能提供更多具体的信息，例如关键词、例子或特定场景，以帮助智税咨询更好地理解你的问题或要求。<br>3.相关背景:如果你的问题或要求需要一些背景信息，提供一些相关背景，这将有助于智税咨询更好地理解问题，并提供更有针对性的回答。<br>4.合理的期望: 你的问题应该合理，并符合产品的能力和限制。<br>例如，一个好的问题可能是:&ldquo;有一张excel表格，需要将T列的值全部取出，并以&ldquo;,&rdquo;分隔，请给出实际的例子。\'这个问题是清晰明确的，提供了具体的要求和背暑信息，同时也是合理的期望，因为智税咨询可以提供相关的信息和例子来回答这个问题。<\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>&nbsp;<\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>四个关键词：主题，细节，背景，期望<\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>又例如：&ldquo;我在处理公司税务时遇到了一些困难，希望能够获得关于如何避免公司税务风险的建议。我们是一家小型企业，我们需要了解如何在合规的情况下最大限度地降低税务负担。请提供建议和指导，包括如何选择最适合我们公司的税务方案，以及如何在税务申报和报告方面遵循最佳实践。此外，请提供一些税务规划的建议，以帮助我们最大化利润，并避免任何潜在的税务问题。&rdquo;<\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>此外，得到初版答案后，我们可以继续对话，使其<span style=\'font-size: 1em;\'>根据我们的要求去<\/span><strong style=\'font-size: 1em;\'>完善细节<\/strong><span style=\'font-size: 1em;\'>。<\/span><\/p>\n<p style=\'text-align: justify; line-height: 1.5;\'>&nbsp;<\/p>','create_time':1679200292,'status':1}}
})

Mock.mock('http://127.0.0.1:8998/api/Redemption/rede', () => {
	return {"status":200,"data":{"avatar":null,"member_id":99,"name":null,"nick_name":"手机用户jNvrtE","num":1039,"openid":null}}
})

Mock.mock('http://127.0.0.1:8998/api/Member/sendSms', () => {
	return {"status":200,"msg":"发送成功","key":"938138000f4834be84af30c0b236aeee"}
})

Mock.mock('http://127.0.0.1:8998/api/Member/add', () => {
	return {"status":800,"msg":"注册成功，可以返回登录"}
})

Mock.mock('http://127.0.0.1:8998/api/Member/login', () => {
	return {"status":200,"data":{"member_id":100,"name":null,"nick_name":"手机用户BRTjKo","avatar":null,"openid":null,"create_time":1680669665,"ip":null,"num":100},"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJjbGllbnQudnVlYWRtaW4iLCJhdWQiOiJzZXJ2ZXIudnVlYWRtaW4iLCJpYXQiOjE2ODA2Njk3MDMuNDAxMjc2LCJuYmYiOjE2ODA2Njk3MDMuNDAxMjc2LCJleHAiOjE2ODExMDE3MDMuNDAxMjc2LCJ1aWQiOjEwMH0.OgKakMtZRw1E23GZS2hFMdZDzhbhTwcWvtkJhWJSMWI"}
})

Mock.mock('http://127.0.0.1:8998/api/Message/sendV2', () => {
	return {"status":"200","data":{"chat_id":"333","content":"您好，有什么财税或法律方面的问题需要我为您解答呢？","role":"assistant","create_time":1680669879,"status":1,"message_id":"789"}}
})


